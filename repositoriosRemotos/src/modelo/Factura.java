/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Date;

/**
 *
 * @author martha.vasquezusam
 */
public class Factura {
    private int numFactura;
    private Date fecha;
    private Cliente idCliente;
    private ModoPago numPago;

    public Factura() {
    }

    public Factura(int numFactura) {
        this.numFactura = numFactura;
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public ModoPago getNumPago() {
        return numPago;
    }

    public void setNumPago(ModoPago numPago) {
        this.numPago = numPago;
    }
    
}
