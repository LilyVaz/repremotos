/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.Categoria;

/**
 *
 * @author martha.vasquezusam
 */
public class CategoriaD {
    Conexion con;
    PreparedStatement ps;
    ResultSet rs;
    String sql;

    public CategoriaD(Conexion con) {
        this.con = con;
    }
    public boolean insert(Categoria b){
        try {
            sql="insert into categoria values(?,?,?)";
            ps=con.conectar().prepareStatement(sql);
            ps.setInt(1, b.getIdCategoria());
            ps.setString(2, b.getNombre());
            ps.setString(3, b.getDescripcion());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean update(Categoria b){
        try {
            sql="update categoria set nombre=?,descripcion=? where id_categoria=?";
            ps=con.conectar().prepareStatement(sql);
            ps.setString(1, b.getNombre());
            ps.setString(2, b.getDescripcion());
            ps.setInt(3, b.getIdCategoria());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean delete(int id){
        try {
            sql="delete from categoria where id_categoria=?";
            ps=con.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public List<Categoria>readId(int id){
        try {
            sql="select * from categoria where id_categoria=?";
            ps=con.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            rs=ps.executeQuery();
            List<Categoria>list=new LinkedList<>();
            while (rs.next()) {
                Categoria b=new Categoria(rs.getInt(1));
                b.setNombre(rs.getString(2));
                b.setDescripcion(rs.getString(3));
                list.add(b);
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }
    public List<Categoria>read(){
        try {
            sql="select * from categoria";
            ps=con.conectar().prepareStatement(sql);
            rs=ps.executeQuery();
            List<Categoria>list=new LinkedList<>();
            while (rs.next()) {
                Categoria b=new Categoria(rs.getInt(1));
                b.setNombre(rs.getString(2));
                b.setDescripcion(rs.getString(3));
                list.add(b);
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }
}
