/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import conexion.Conexion;
import dao.CategoriaD;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Categoria;

/**
 *
 * @author martha.vasquezusam
 */
public class CategoriaC extends HttpServlet {

    Conexion con = new Conexion();
    CategoriaD dao = new CategoriaD(con);
    RequestDispatcher rd;
    List<Categoria>list;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        try {
            String action = request.getParameter("action");
            switch (action) {
                case "insert":
                    insert(request, response);
                    break;
                case "update":
                    update(request, response);
                    break;
                case "delete":
                    delete(request, response);
                    break;
                case "readId":
                    readId(request, response);
                    break;
                case "read":
                    read(request, response);
                    break;
                default:
                    throw new AssertionError();
            }
        } catch (Exception e) {
            System.out.println("Error de action ");
        }
    }

    protected void insert(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        try {
            String nombre=request.getParameter("nombre");
            String descripcion=request.getParameter("descripcion");
            Categoria b=new Categoria(0);
            b.setNombre(nombre);
            b.setDescripcion(descripcion);
            dao.insert(b);
            rd=request.getRequestDispatcher("/registro.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
        }
    }

    protected void update(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        try {
            int idCategoria=Integer.parseInt("idCategoria");
            String nombre=request.getParameter("nombre");
            String descripcion=request.getParameter("descripcion");
            Categoria b=new Categoria(idCategoria);
            b.setNombre(nombre);
            b.setDescripcion(descripcion);
            dao.update(b);
            list=dao.read();
            request.setAttribute("list", list);
            rd=request.getRequestDispatcher("/mostrar.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
        }
    }

    protected void delete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        try {
            int idCategoria=Integer.parseInt("idCategoria");
            dao.delete(idCategoria);            
            list=dao.read();
            request.setAttribute("list", list);
            rd=request.getRequestDispatcher("/mostrar.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
        }
    }

    protected void readId(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        try {
            int idCategoria=Integer.parseInt("idCategoria");
            list=dao.readId(idCategoria);  
            request.setAttribute("list", list);
            rd=request.getRequestDispatcher("/actualizar.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
        }
    }

    protected void read(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        try {
            list=dao.read();  
            request.setAttribute("list", list);
            rd=request.getRequestDispatcher("/mostrar.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CategoriaC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CategoriaC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
