/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author martha.vasquezusam
 */
public class Conexion {
    private String user="root";
    private String pass="root";
    private String url="jdbc:mysql://localhost:3306/participante5?useSSL=false";
    Connection con=null;

    public Conexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection(url, user, pass);
            if (con!=null) {
                System.out.println("Exito de conexión");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }
    public Connection conectar(){
        return con;
    }
    public void desconectar() throws Exception{
        con.close();
    }
}
